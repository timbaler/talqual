## ----------------------------------------------------------------------
## Talqual Makefile Help.
## ----------------------------------------------------------------------

.PHONY: help
help:   ## Show this help.
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST)

.PHONY: test
test:   ## Run all tests (except selenium) including coverage and flake8
        ## You can select tests to run: make test args="-k test_simple"
	pytest $(args)

.PHONY: testall
testall: transcrypt ## Run all tests
		    ## including selenium (firefox), coverage and flake8
	pytest $(args) --driver firefox

.PHONY: testcov
testcov:
	pytest --cov-report=html
	@echo "open file://`pwd`/htmlcov/index.html"

.PHONY: doc
doc:
	make -C docs html
	@echo "open file://`pwd`/docs/_build/html/index.html"

venv:
	python3 -m venv .
	@echo "Run source bin/activate for enabling venv"

.PHONY: dev
dev: venv
	./bin/pip install --upgrade --upgrade-strategy eager -r requirements-dev.txt

.PHONY: transcrypt
transcrypt: ## Transpile scripts to talqual/static
	transcrypt ./scripts/
	cp ./__target__/*.js ./talqual/static/
