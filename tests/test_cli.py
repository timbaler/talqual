from click.testing import CliRunner

from talqual.cli import cli


def test_cli_help():
    runner = CliRunner()
    result = runner.invoke(cli, ['--help'])
    assert result.exit_code == 0
    assert 'Usage:' in result.output
    assert 'build' in result.output


def test_cli_version():
    runner = CliRunner()
    result = runner.invoke(cli, ['--version'])
    assert result.exit_code == 0
    assert 'talqual, version' in result.output


def test_build(mocker, tmp_path):
    src = str(tmp_path)
    dst = str(tmp_path / 'output')
    data = tmp_path / 'data'
    data.mkdir()
    data_yaml = tmp_path / 'data.yaml'
    data_yaml.write_text('title: talqual')
    data = str(data)
    data_yaml = str(data_yaml)
    locales_dir = tmp_path / 'locales'
    locales_dir.mkdir()

    mock_build = mocker.patch('talqual.cli.main.build')
    runner = CliRunner()

    result = runner.invoke(cli, ['build', src])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['--verbose', 'build', src])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['build', src, dst])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['build', src, '--data', data])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['build', src, dst, '--data', data])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['build', src, dst, '--data', data_yaml])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['build', src, dst, '--locales', locales_dir])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    result = runner.invoke(cli, ['build', src, dst, '--data', data_yaml,
                                 '--locales', locales_dir])
    assert result.exit_code == 0, result.output
    assert '' == result.output

    assert mock_build.call_count == 8
