from talqual.main import build


template_value = '<html><p tal:content="variable1">Test</p></html>'
template2_value = '<html><p tal:content="data.variable1">Test</p></html>'
template_render = '<html><p>Template rendered</p></html>'
data_value = 'variable1: Template rendered'


def test_simple_build(tmp_path):
    data = tmp_path / 'data.yaml'
    data.write_text(data_value)

    templates = tmp_path / 'templates'
    templates.mkdir()
    index = templates / 'index.html'
    index.write_text(template_value)

    output = tmp_path / 'output'
    output.mkdir()

    build(templates, output, data)

    assert output.exists()
    index_html = output / 'index.html'
    assert index_html.exists()
    assert index_html.read_text() == template_render


def test_data_build(tmp_path):
    data_root = tmp_path / 'data'
    data_root.mkdir()
    data = data_root / 'data.yaml'
    data.write_text(data_value)

    templates = tmp_path / 'templates'
    templates.mkdir()
    index = templates / 'index.html'
    index.write_text(template2_value)

    output = tmp_path / 'output'
    output.mkdir()

    build(templates, output, data_root)

    assert output.exists()
    index_html = output / 'index.html'
    assert index_html.exists()
    assert index_html.read_text() == template_render


def test_build_without_data(tmp_path):
    templates = tmp_path / 'templates'
    templates.mkdir()
    index = templates / 'index.html'
    index.write_text(template_render)

    output = tmp_path / 'output'
    output.mkdir()

    build(templates, output, data_src=None)

    assert output.exists()
    index_html = output / 'index.html'
    assert index_html.exists()
    assert index_html.read_text() == template_render
