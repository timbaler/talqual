from anytree import Node

from talqual.templates import Folder, HtmlPt, Template


def test_list_folder_contents():
    f = Folder('folder')
    i = HtmlPt('index.html', parent=f)
    i.value = (
        """<html tal:define="context {'title': 'I'}; """
        'items view.parent.children;">'
        '<a tal:repeat="item items" href="${url(item.relative_path())}" '
        'tal:content="item.context.title|item.name"></a></html>')
    p = HtmlPt('page.html', parent=f)
    p.value = """<html tal:define="context {'title': 'A Page'};"></html>"""

    view = f.render({})
    i_html = view.children[0]
    out = '<html><a href="">I</a>\n<a href="page.html">A Page</a></html>'
    assert i_html.value == out


def test_list_folder_contents_with_tal_repeat():
    root = Node('folder')
    i = Node('index.html', parent=root)
    i.value = (
        """<html tal:define="context {'title': 'I'}; """
        'items view.parent.children;">'
        '<a tal:repeat="item items" href="${url(item.relative_path())}">'
        '<i tal:omit-tag="" tal:content="item.context.num|nothing"></i>'
        '<i tal:omit-tag="" tal:content="item.context.title|item.name"></i>'
        '</a></html>')
    p = Node('tal_.tal_repeat_contents.html', parent=root)
    p.value = (
        """<html tal:define="context {'num': tal_repeat_contents.get('num'),"""
        """'title': contents[tal_repeat_contents.get('item')]};">"""
        '<a href="${url: folder}">${context.num}</a></html>')

    data = {'contents': {'page1': 'A Page 1', 'page2': 'A Page 2'}}

    templates = Template.template_from_tree(root)
    view = templates.render(data)
    i_html = view.children[0]
    out = ('<html><a href="">I</a>\n<a href="tal_.tal_repeat_contents.html">'
           'tal_.tal_repeat_contents.html</a></html>')
    assert i_html.value == out
    p1_html = view.children[1]
    assert p1_html.value == '<html><a href="">0</a></html>'
    p2_html = view.children[2]
    assert p2_html.value == '<html><a href="">1</a></html>'

    view.render(data)
    i_html = view.children[0]
    out = ('<html><a href="">I</a>\n<a href="page1.html">'
           '0A Page 1</a>\n<a href="page2.html">1A Page 2</a></html>')
    assert i_html.value == out
    p1_html = view.children[1]
    assert p1_html.value == '<html><a href="">0</a></html>'
    p2_html = view.children[2]
    assert p2_html.value == '<html><a href="">1</a></html>'


def test_tal_repeat_folder():
    root = Node('root')
    f = Node('tal_.tal_repeat_folders', parent=root)
    i = Node('index.html', parent=f)
    i.value = '<html><p>${tal_repeat_folders.item}</p></html>'

    data = {'folders': ['folder1', 'folder2']}

    templates = Template.template_from_tree(root)

    view = templates.render(data)
    i_html = view.children[0].children[0]
    assert i_html.value == '<html><p>folder1</p></html>'
    i2_html = view.children[1].children[0]
    assert i2_html.value == '<html><p>folder2</p></html>'

    view.render(data)
    i_html = view.children[0].children[0]
    assert i_html.value == '<html><p>folder1</p></html>'
    i2_html = view.children[1].children[0]
    assert i2_html.value == '<html><p>folder2</p></html>'
