from talqual.importer import FileSystemImporter
from talqual.templates import HtmlPt, Template


def test_get_context():
    t = HtmlPt('index.html')
    t.value = ("""<html tal:define="context {'title': 'A',"""
               """'description': 'B'};">"""
               '<p tal:content="context.title"></p></html>')

    assert t.context == {'title': 'A', 'description': 'B'}


def test_get_context_from_data():
    t = HtmlPt('index.html')
    t.value = ("""<html tal:define="context {'title': title};">"""
               '<p tal:content="context.title"></p></html>')

    data = {'title': 'A'}
    view = t.render(data)
    assert view.value == '<html><p>A</p></html>'
    assert t.context == {'title': 'A'}
    assert view.context == {'title': 'A'}


def test_get_context_from_file(tmp_path):
    templates = tmp_path / 'templates'
    templates.mkdir()
    index = templates / 'index.html'
    t_value = ("""<html tal:define="context {'title': 'A',"""
               """'description': 'B'};">"""
               '<p tal:content="context.title"></p></html>')
    index.write_text(t_value)

    importer = FileSystemImporter()
    root = importer.import_(templates)
    templates = Template.template_from_tree(root)

    index_template = templates.children[0]
    assert index_template.context == {'title': 'A', 'description': 'B'}
