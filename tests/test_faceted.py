from anytree import Node
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

from talqual.templates import Template
from tests.conftest import driver


# templates
root = Node('templates')
script = Node('scripts.tal_replace_talqual_scripts.js', parent=root)
template = Node('template.pt', parent=root)
template.value = """<html><head><script type="module">
import { Faceted } from './scripts.js'; window.Facet = Faceted("batch");
</script></head>
<body>
<ul class="batch">
<li tal:repeat="i news" class="type-${i.type}">${i.name}</li>
</ul>
<select name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""
template_render = """<html><head><script type="module">
import { Faceted } from './scripts.js'; window.Facet = Faceted("batch");
</script></head>
<body>
<ul class="batch">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
<li class="type-C">Item 3</li>
<li class="type-A">Item 4</li>
<li class="type-B">Item 5</li>
<li class="type-C">Item 6</li>
<li class="type-A">Item 7</li>
</ul>
<select name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

faceted_render = """<html><head><script type="module">
import { Faceted } from './scripts.js'; window.Facet = Faceted("batch");
</script></head>
<body>
<ul class="batch">
<li class="type-A">Item 1</li>
<li class="type-B" style="display: none;">Item 2</li>
<li class="type-C" style="display: none;">Item 3</li>
<li class="type-A">Item 4</li>
<li class="type-B" style="display: none;">Item 5</li>
<li class="type-C" style="display: none;">Item 6</li>
<li class="type-A">Item 7</li>
</ul>
<select name="type" onchange="window.Facet.filter(this)" style="">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

faceted_multiselect_render = """<html><head><script type="module">
import { Faceted } from './scripts.js'; window.Facet = Faceted("batch");
</script></head>
<body>
<ul class="batch">
<li class="type-A">Item 1</li>
<li class="type-B" style="">Item 2</li>
<li class="type-C" style="display: none;">Item 3</li>
<li class="type-A">Item 4</li>
<li class="type-B" style="">Item 5</li>
<li class="type-C" style="display: none;">Item 6</li>
<li class="type-A">Item 7</li>
</ul>
<select multiple="" name="type" onchange="window.Facet.filter(this)" style="">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

select_virtual = """<html><head><script type="module">
import { Faceted, VirtualSelect } from './scripts.js';
window.Facet = Faceted("batch");
window.VirtualSelect = VirtualSelect("type", window.Facet);
</script></head>
<body>
<ul class="batch">
<li tal:repeat="i news" class="type-${i.type}">${i.name}</li>
</ul>
<button onclick="window.VirtualSelect.toggle('A')" id="A">A</button>
<button onclick="window.VirtualSelect.toggle('B')" id="B">B</button>
<button onclick="window.VirtualSelect.toggle('C')">C</button>

</body></html>"""
select_virtual_html = """<html><head><script type="module">
import { Faceted, VirtualSelect } from './scripts.js';
window.Facet = Faceted("batch");
window.VirtualSelect = VirtualSelect("type", window.Facet);
</script></head>
<body>
<ul class="batch">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
<li class="type-C">Item 3</li>
<li class="type-A">Item 4</li>
<li class="type-B">Item 5</li>
<li class="type-C">Item 6</li>
<li class="type-A">Item 7</li>
</ul>
<button onclick="window.VirtualSelect.toggle('A')" id="A">A</button>
<button onclick="window.VirtualSelect.toggle('B')" id="B">B</button>
<button onclick="window.VirtualSelect.toggle('C')">C</button>

</body></html>"""
select_virtual_render = """<html><head><script type="module">
import { Faceted, VirtualSelect } from './scripts.js';
window.Facet = Faceted("batch");
window.VirtualSelect = VirtualSelect("type", window.Facet);
</script></head>
<body>
<ul class="batch">
<li class="type-A">Item 1</li>
<li class="type-B" style="">Item 2</li>
<li class="type-C" style="display: none;">Item 3</li>
<li class="type-A">Item 4</li>
<li class="type-B" style="">Item 5</li>
<li class="type-C" style="display: none;">Item 6</li>
<li class="type-A">Item 7</li>
</ul>
<button onclick="window.VirtualSelect.toggle('A')" id="A" style="">A</button>
<button onclick="window.VirtualSelect.toggle('B')" id="B" style="">B</button>
<button onclick="window.VirtualSelect.toggle('C')">C</button>

</body></html>"""

# data
data = {
    'news': [
        {'id': 'i1', 'name': 'Item 1', 'type': 'A'},
        {'id': 'i2', 'name': 'Item 2', 'type': 'B'},
        {'id': 'i3', 'name': 'Item 3', 'type': 'C'},
        {'id': 'i4', 'name': 'Item 4', 'type': 'A'},
        {'id': 'i5', 'name': 'Item 5', 'type': 'B'},
        {'id': 'i6', 'name': 'Item 6', 'type': 'C'},
        {'id': 'i7', 'name': 'Item 7', 'type': 'A'},
        ],
    }


@driver
def test_faceted(tmp_path, selenium):
    templates = Template.template_from_tree(root)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    assert directory.exists()
    assert len(list(directory.iterdir())) == 3
    script_js = directory / 'scripts.js'
    assert script_js.exists()
    template_html = directory / 'template.html'
    assert template_html.exists()
    assert template_html.read_text() == template_render

    selenium.get(f'file://{template_html}')
    select = Select(selenium.find_elements(By.NAME, 'type')[0])
    select.select_by_value('A')

    assert selenium.page_source == faceted_render

    select.select_by_value('')
    assert selenium.page_source.replace(' style=""', '') == template_render


@driver
def test_faceted_multiselect(tmp_path, selenium):
    template.value = template.value.replace('<select', '<select multiple')

    templates = Template.template_from_tree(root)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    template_html = directory / 'template.html'
    assert template_html.exists()

    selenium.get(f'file://{template_html}')
    select = Select(selenium.find_elements(By.NAME, 'type')[0])
    select.select_by_value('A')
    select.select_by_value('B')
    assert len(select.all_selected_options) == 2

    assert selenium.page_source == faceted_multiselect_render

    template.value = template.value.replace('<select multiple', '<select')


@driver
def test_faceted_multiselect_virtual(tmp_path, selenium):
    original = template.value
    template.value = select_virtual

    templates = Template.template_from_tree(root)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    template_html = directory / 'template.html'
    assert template_html.exists()
    assert template_html.read_text() == select_virtual_html

    selenium.get(f'file://{template_html}')
    buttonA = selenium.find_element(By.ID, 'A')
    buttonB = selenium.find_element(By.ID, 'B')
    buttonA.click()
    buttonB.click()

    assert selenium.page_source == select_virtual_render

    buttonA.click()
    buttonB.click()
    assert selenium.page_source.replace(' style=""', '') == select_virtual_html

    template.value = original


# faceted + batch
root2 = Node('templates')
script2 = Node('scripts.tal_replace_talqual_scripts.js', parent=root2)
template2 = Node('template.tal_batch_news_2.pt', parent=root2)
template2.value = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", ${news.batch.size}, "pagination");
</script></head>
<body>
<ul class="batch">
<li tal:repeat="i news" class="type-${i.type}">${i.name}</li>
</ul>
<p class="pagination" tal:define="batch news">
  <a href="${batch.get_url(batch.prev)}" rel="prev">Previous</a>
  <a href="${batch.get_url(p+1)}" tal:repeat="p range(batch.last)">${p}</a>
  <a href="${batch.get_url(batch.next)}" rel="next">Next</a>
</p>
<select name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_html = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 2, "pagination");
</script></head>
<body>
<ul class="batch">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul>
<p class="pagination">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>
<select name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_render = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 2, "pagination");
</script></head>
<body>
<ul class="batch" style="display: none;">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul><ul class="batch" style=""><li class="type-A">Item 1</li>\
<li class="type-A">Item 4</li></ul>
<p class="pagination" style="display: none;">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>\
<p class="pagination" style=""><a rel="next">Next</a></p>
<select name="type" onchange="window.Facet.filter(this)" style="">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_render_page_2 = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 2, "pagination");
</script></head>
<body>
<ul class="batch" style="display: none;">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul><ul class="batch"><li class="type-A">Item 7</li></ul>
<p class="pagination" style="display: none;">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>\
<p class="pagination"><a rel="next">Next</a></p>
<select name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_render_4 = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 4, "pagination");
</script></head>
<body>
<ul class="batch" style="display: none;">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul><ul class="batch"><li class="type-A">Item 1</li>\
<li class="type-A">Item 4</li><li class="type-A">Item 7</li></ul>
<p class="pagination" style="display: none;">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>\
<p class="pagination"><a rel="next">Next</a></p>
<select name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_render_multiselect = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 2, "pagination");
</script></head>
<body>
<ul class="batch" style="display: none;">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul><ul class="batch" style=""><li class="type-A">Item 1</li>\
<li class="type-B">Item 2</li></ul>
<p class="pagination" style="display: none;">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>\
<p class="pagination" style=""><a rel="next">Next</a></p>
<select multiple="" name="type" onchange="window.Facet.filter(this)" style="">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_render_multiselect_page_2 = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 2, "pagination");
</script></head>
<body>
<ul class="batch" style="display: none;">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul><ul class="batch"><li class="type-A">Item 4</li>\
<li class="type-B">Item 5</li></ul>
<p class="pagination" style="display: none;">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>\
<p class="pagination"><a rel="next">Next</a></p>
<select multiple="" name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""

facebatch_render_multiselect_page_3 = """<html><head><script type="module">
import { Faceted } from './scripts.js';
window.Facet = Faceted("batch", 2, "pagination");
</script></head>
<body>
<ul class="batch" style="display: none;">
<li class="type-A">Item 1</li>
<li class="type-B">Item 2</li>
</ul><ul class="batch"><li class="type-A">Item 7</li></ul>
<p class="pagination" style="display: none;">
  <a rel="prev">Previous</a>
  <a href="template.html">0</a>
  <a href="template.2.html">1</a>
  <a href="template.3.html">2</a>
  <a href="template.4.html">3</a>
  <a href="template.2.html" rel="next">Next</a>
</p>\
<p class="pagination"><a rel="next">Next</a></p>
<select multiple="" name="type" onchange="window.Facet.filter(this)">
  <option value="">All</option>
  <option value="A">Type A</option>
  <option value="B">Type B</option>
  <option value="C">Type C</option>
</select>

</body></html>"""


@driver
def test_faceted_batch(tmp_path, selenium):
    templates = Template.template_from_tree(root2)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    assert len(list(directory.iterdir())) == 6
    template_html = directory / 'template.html'
    assert template_html.read_text() == facebatch_html

    selenium.get(f'file://{template_html}')
    select = Select(selenium.find_elements(By.NAME, 'type')[0])
    select.select_by_value('A')
    assert selenium.page_source == facebatch_render

    next_link = selenium.find_elements(
        By.CSS_SELECTOR, '.pagination [rel="next"]')[-1]
    next_link.click()
    assert selenium.page_source.replace(
        ' style=""', '') == facebatch_render_page_2

    # no more pages
    next_link = selenium.find_elements(
        By.CSS_SELECTOR, '.pagination [rel="next"]')[-1]
    next_link.click()
    assert selenium.page_source.replace(
        ' style=""', '') == facebatch_render_page_2

    select.select_by_value('')
    assert selenium.page_source.replace(' style=""', '') == facebatch_html


@driver
def test_faceted_batch_size_4(tmp_path, selenium):
    template2.value = template2.value.replace('${news.batch.size}', '4')

    templates = Template.template_from_tree(root2)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    template_html = directory / 'template.html'

    selenium.get(f'file://{template_html}')
    select = Select(selenium.find_elements(By.NAME, 'type')[0])
    select.select_by_value('A')
    assert selenium.page_source.replace(' style=""', '') == facebatch_render_4

    # no more pages
    next_link = selenium.find_elements(
        By.CSS_SELECTOR, '.pagination [rel="next"]')[-1]
    next_link.click()
    assert selenium.page_source.replace(' style=""', '') == facebatch_render_4

    select.select_by_value('')
    assert selenium.page_source.replace(
        ' style=""', '') == facebatch_html.replace(', 2,', ', 4,')

    template2.value = template2.value.replace(', 4,', ', ${news.batch.size},')


@driver
def test_faceted_batch_multiselect(tmp_path, selenium):
    template2.value = template2.value.replace('<select', '<select multiple')

    templates = Template.template_from_tree(root2)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    template_html = directory / 'template.html'
    assert template_html.exists()

    selenium.get(f'file://{template_html}')
    select = Select(selenium.find_elements(By.NAME, 'type')[0])
    select.select_by_value('A')
    select.select_by_value('B')
    assert len(select.all_selected_options) == 2
    assert selenium.page_source == facebatch_render_multiselect

    # page 2
    next_link = selenium.find_elements(
        By.CSS_SELECTOR, '.pagination [rel="next"]')[-1]
    next_link.click()
    assert selenium.page_source.replace(
        ' style=""', '') == facebatch_render_multiselect_page_2

    # page 3
    next_link = selenium.find_elements(
        By.CSS_SELECTOR, '.pagination [rel="next"]')[-1]
    next_link.click()
    assert selenium.page_source.replace(
        ' style=""', '') == facebatch_render_multiselect_page_3

    # no more pages
    next_link = selenium.find_elements(
        By.CSS_SELECTOR, '.pagination [rel="next"]')[-1]
    next_link.click()
    assert selenium.page_source.replace(
        ' style=""', '') == facebatch_render_multiselect_page_3

    template2.value = template2.value.replace('<select multiple', '<select')
