import datetime

from anytree import Node

from talqual.templates import Template
from tests.conftest import driver


# time
now = datetime.datetime.now()
day1 = datetime.timedelta(days=1)


class datetimepast:
    @classmethod
    def now(cls):
        return now-day1


# templates
root = Node('templates')
script = Node('scripts.tal_replace_talqual_scripts.js', parent=root)
template = Node('template.pt', parent=root)
template.value = """<html><head><script type="module">
import { Calendar } from './scripts.js'; Calendar("calendar");
</script></head>
<body>
Future:
<ul class="calendar"
tal:define="datetime import: datetime; now python:datetime.datetime.now()">
<tal:repeat repeat="i sorted(news,key=lambda x: x.date)">\
<li data-date="${i.date}" tal:condition="now<i.date">${i.name}</li>
</tal:repeat>\
</ul>
</body></html>"""

template_render = f"""<html><head><script type="module">
import {{ Calendar }} from './scripts.js'; Calendar("calendar");
</script></head>
<body>
Future:
<ul class="calendar">




<li data-date="{now+day1}">Item 2</li>
<li data-date="{now+day1*2}">Item 4</li>
<li data-date="{now+day1*3}">Item 5</li>
</ul>
</body></html>"""

template2_render = f"""<html><head><script type="module">
import {{ Calendar }} from './scripts.js'; Calendar("calendar");
</script></head>
<body>
Future:
<ul class="calendar">



<li data-date="{now}">Item 1</li>
<li data-date="{now+day1}">Item 2</li>
<li data-date="{now+day1*2}">Item 4</li>
<li data-date="{now+day1*3}">Item 5</li>
</ul>
</body></html>"""

template3_render = f"""<html><head><script type="module">
import {{ Calendar }} from './scripts.js'; Calendar("calendar");
</script></head>
<body>
Future:
<ul class="calendar">



<li data-date="{now}" style="display: none;">Item 1</li>
<li data-date="{now+day1}">Item 2</li>
<li data-date="{now+day1*2}">Item 4</li>
<li data-date="{now+day1*3}">Item 5</li>
</ul>
</body></html>"""

# data
data = {
    'news': [
        {'id': 'i1', 'name': 'Item 1', 'date': now},
        {'id': 'i2', 'name': 'Item 2', 'date': now+day1},
        {'id': 'i3', 'name': 'Item 3', 'date': now-day1},
        {'id': 'i4', 'name': 'Item 4', 'date': now+2*day1},
        {'id': 'i5', 'name': 'Item 5', 'date': now+3*day1},
        {'id': 'i6', 'name': 'Item 6', 'date': now-2*day1},
        {'id': 'i7', 'name': 'Item 7', 'date': now-3*day1},
        ],
    }


@driver
def test_calendar(tmp_path, selenium, monkeypatch):
    templates = Template.template_from_tree(root)
    view = templates.render(data)
    directory = tmp_path / 'html'
    view.write(directory)

    template_html = directory / 'template.html'
    assert len(list(directory.iterdir())) == 3
    assert template_html.exists()
    assert template_html.read_text() == template_render

    # change time to past
    monkeypatch.setattr(datetime, 'datetime', datetimepast)
    view = templates.render(data)
    view.write(directory)
    template_html = directory / 'template.html'
    assert template_html.read_text() == template2_render

    selenium.get(f'file://{template_html}')
    assert selenium.page_source == template3_render

    # check no more templates than original are generated
    assert len(templates.children) == 2  # templates remain as original
    assert len(view.children) == 3  # one tempate replaced with 2 scripts
