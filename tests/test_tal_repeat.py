from anytree import Node, RenderTree

from talqual.templates import Template


# templates
root = Node('t')
news = Node('news.tal_repeat_langs', parent=root)  # repeated folder
index = Node('i.html', parent=news)
index.value = '${tal_repeat_langs} ${url: ca}'
lang = Node('tal_.tal_repeat_langs', parent=root)  # repeated folder by name
index2 = Node('i.html', parent=lang)
index2.value = '${tal_repeat_langs}'

# data
data = {
    'langs': ['ca', 'en', 'oc'],
    }


template_str = f"""\
Folder('/t')
├── TalCommand('/t/news.tal_repeat_langs')
│   └── HtmlPt('/t/news.tal_repeat_langs/i.html', value='{index.value}')
└── TalCommand('/t/tal_.tal_repeat_langs')
    └── HtmlPt('/t/tal_.tal_repeat_langs/i.html', value='{index2.value}')\
"""

view_str = """\
View('/t')
├── View('/t/news.0')
│   └── ViewHtml('/t/news.0/i.html', value="{'num': 0, 'item': 'ca'} ../ca")
├── View('/t/news.1')
│   └── ViewHtml('/t/news.1/i.html', value="{'num': 1, 'item': 'en'} ../ca")
├── View('/t/news.2')
│   └── ViewHtml('/t/news.2/i.html', value="{'num': 2, 'item': 'oc'} ../ca")
├── View('/t/ca')
│   └── ViewHtml('/t/ca/i.html', value="{'num': 0, 'item': 'ca'}")
├── View('/t/en')
│   └── ViewHtml('/t/en/i.html', value="{'num': 1, 'item': 'en'}")
└── View('/t/oc')
    └── ViewHtml('/t/oc/i.html', value="{'num': 2, 'item': 'oc'}")\
"""

view_nocontent_str = """\
View('/templates')
├── View('/templates/news.0')
├── View('/templates/news.1')
└── View('/templates/news.2')\
"""


def test_repeat_folder():
    templates = Template.template_from_tree(root)
    assert str(RenderTree(templates)) == template_str

    view = templates.render(data)
    assert str(RenderTree(view)) == view_str


def test_folder_without_content():
    root = Node('templates')
    Node('news.tal_repeat_langs', parent=root)
    templates = Template.template_from_tree(root)
    view = templates.render(data)
    assert str(RenderTree(view)) == view_nocontent_str
    assert len(data) == 1  # tal_repeat_langs variable deleted
