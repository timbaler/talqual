import json

import babel.messages
import babel.messages.mofile
from anytree import Node, RenderTree

from talqual.main import build
from talqual.templates import Template
from talqual.translation import Translation


# templates
root = Node('t')
static = Node('s.html', parent=root)
static.value = (
    '<html i18n:domain="tal.qual"><body><p i18n:translate="">Hello world!</p>'
    '<p i18n:translate="search_id">Another</p></body></html>')
static_en = '<html><body><p>Hello world!</p><p>Another</p></body></html>'
lang = Node('tal_.tal_repeat_LOCALES', parent=root)  # repeated by LOCALES
index = Node('i.html', parent=lang)
index.value = (
    '<html i18n:domain="tal.qual"><body>${tal_repeat_LOCALES.item}'
    '<p i18n:translate="">Hello world!</p></body></html>')
index_ca = '<html><body>ca<p>Bon dia</p></body></html>'
index_en = '<html><body>en<p>Hello world!</p></body></html>'
index_oc = '<html><body>oc<p>Adieu-siatz</p></body></html>'
index2 = Node('i2.html', parent=lang)
index2.value = (
    '<html i18n:domain="tal.qual" tal:define="date import: datetime.date">'
    '<body>${date(2017,10,1)}<p i18n:translate="copyright">&copy;<i '
    'i18n:name="year">${date(2017,10,1).year}</i></p></body></html>')
index2_str = '<html><body>2017-10-01<p>&copy;<i>2017</i></p></body></html>'

# data
data = {
    'LOCALES': ['ca', 'en', 'oc'],
    }


template_str = f"""\
Folder('/t')
├── HtmlPt('/t/s.html', value='{static.value}')
└── TalCommand('/t/tal_.tal_repeat_LOCALES')
    ├── HtmlPt('/t/tal_.tal_repeat_LOCALES/i.html', value='{index.value}')
    └── HtmlPt('/t/tal_.tal_repeat_LOCALES/i2.html', value='{index2.value}')\
"""

view_str = f"""\
View('/t')
├── ViewHtml('/t/s.html', value='{static_en}')
├── View('/t/ca')
│   ├── ViewHtml('/t/ca/i.html', value='{index_ca}')
│   └── ViewHtml('/t/ca/i2.html', value='{index2_str}')
├── View('/t/en')
│   ├── ViewHtml('/t/en/i.html', value='{index_en}')
│   └── ViewHtml('/t/en/i2.html', value='{index2_str}')
└── View('/t/oc')
    ├── ViewHtml('/t/oc/i.html', value='{index_oc}')
    └── ViewHtml('/t/oc/i2.html', value='{index2_str}')\
"""


def create_locales(tmp_path):
    locales = tmp_path / 'locales_example'
    locales.mkdir()

    catalog_ca = babel.messages.Catalog(locale='ca')
    catalog_ca.add('Hello world!', 'Bon dia')
    catalog_en = babel.messages.Catalog(locale='en')
    catalog_en.add('Hello world!', '')
    catalog_oc = babel.messages.Catalog(locale='oc')
    catalog_oc.add('Hello world!', 'Adieu-siatz')

    langs = {'ca': catalog_ca, 'en': catalog_en, 'oc': catalog_oc}

    for lang, catalog in langs.items():
        folder_lang = locales / lang
        folder_lang.mkdir()
        messages = folder_lang / 'LC_MESSAGES'
        messages.mkdir()
        mo_lang = messages / 'tal.qual.mo'
        with mo_lang.open('wb') as f:
            babel.messages.mofile.write_mo(f, catalog)

    return locales


def test_translation_folder(tmp_path):
    locales = create_locales(tmp_path)
    templates = Template.template_from_tree(root)
    assert str(RenderTree(templates)) == template_str

    templates.set_locales(Translation(locales))
    view = templates.render(data)
    assert str(RenderTree(view)) == view_str


def test_translation_build(tmp_path):
    datajson = tmp_path / 'data.json'
    datajson.write_text(json.dumps(data))

    templates = tmp_path / 'templates'
    templates.mkdir()

    static_index = templates / 'i.html'
    static_index.write_text(static.value)

    lang = templates / 'tal_.tal_repeat_LOCALES'
    lang.mkdir()
    lang_index = lang / 'index.html'
    lang_index.write_text(index.value)

    # without locales domain, default translation en
    output = tmp_path / 'output'
    output.mkdir()
    build(templates, output, datajson)
    assert output.exists()
    static_index = output / 'i.html'
    assert static_index.exists()
    assert static_index.read_text() == static_en
    lang_index_ca = output / 'ca' / 'index.html'
    assert lang_index_ca.exists()
    assert lang_index_ca.read_text() == index_en.replace('en', 'ca')
    lang_index_en = output / 'en' / 'index.html'
    assert lang_index_en.exists()
    assert lang_index_en.read_text() == index_en
    lang_index_oc = output / 'oc' / 'index.html'
    assert lang_index_oc.exists()
    assert lang_index_oc.read_text() == index_en.replace('en', 'oc')

    # with locales domain
    locales = create_locales(tmp_path)
    output2 = tmp_path / 'output2'
    output2.mkdir()
    build(templates, output2, datajson, locales=locales)
    assert output2.exists()
    static_index = output / 'i.html'
    assert static_index.exists()
    assert static_index.read_text() == static_en
    lang_index_ca = output2 / 'ca' / 'index.html'
    assert lang_index_ca.exists()
    assert lang_index_ca.read_text() == index_ca
    lang_index_en = output2 / 'en' / 'index.html'
    assert lang_index_en.exists()
    assert lang_index_en.read_text() == index_en
    lang_index_oc = output2 / 'oc' / 'index.html'
    assert lang_index_oc.exists()
    assert lang_index_oc.read_text() == index_oc
