from anytree import Node, RenderTree, search

from talqual.templates import Template


def test_hidden_template():
    data = {}
    root = Node('templates')
    hidden_file = Node('_hidden.txt', parent=root)
    hidden_folder = Node('_hidden', parent=root)
    folder_file = Node('file.txt', parent=hidden_folder)

    assert search.findall_by_attr(root, hidden_file.name, 'name')
    assert search.findall_by_attr(root, hidden_folder.name, 'name')
    assert search.findall_by_attr(root, folder_file.name, 'name')

    templates = Template.template_from_tree(root)
    view = templates.render(data)
    assert str(RenderTree(view)) == "View('/templates')"
    assert len(view.children) == 0
    assert not search.findall_by_attr(view, hidden_file.name, 'name')
    assert not search.findall_by_attr(view, hidden_folder.name, 'name')
    assert not search.findall_by_attr(view, folder_file.name, 'name')


myscripts_str = """\
Folder('/templates')
└── TalCommand('/templates/myscripts.tal_replace_talqual_scripts.js')\
"""


def test_replace_talqual_script():
    data = {}
    root = Node('templates')
    Node('myscripts.tal_replace_talqual_scripts.js', parent=root)

    templates = Template.template_from_tree(root)
    assert str(RenderTree(templates)) == myscripts_str

    view = templates.render(data)
    js = "View('/templates/myscripts.js', value=b\'\\\'use strict\\\';import"
    js2 = ("View('/templates/org.transcrypt.__runtime__.js', value=b\'\\\'"
           "use strict\\\';var __name__=\"org.transcrypt.__runtime__")
    assert js in str(RenderTree(view))
    assert js2 in str(RenderTree(view))


def test_template_path():
    root = Template('templates')
    folder = Template('folder', parent=root)
    a = Template('a.html', parent=folder)

    assert a.full_path() == '/templates/folder/a.html'
    assert a.relative_path() == 'templates/folder/a.html'

    root.name = ''
    assert a.full_path() == '//folder/a.html'
    assert a.relative_path() == 'folder/a.html'
