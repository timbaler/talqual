import json

from anytree import Node, RenderTree

from talqual.data import Json
from talqual.templates import Template


# templates
root = Node('templates')
template = Node('template.pt', parent=root)
template.value = '<html><ul><li tal:repeat="i news">${i.name}</li></ul></html>'
template_render = """<html><ul><li>Item 1</li>
<li>Item 2</li>
<li>Item 3</li>
<li>Item 4</li>
<li>Item 5</li>
<li>Item 6</li>
<li>Item 7</li></ul></html>"""
template_1_render = '<html><ul><li>Item 1</li>\n<li>Item 2</li></ul></html>'
template_2_render = '<html><ul><li>Item 3</li>\n<li>Item 4</li></ul></html>'
template_3_render = '<html><ul><li>Item 5</li>\n<li>Item 6</li></ul></html>'
template_4_render = '<html><ul><li>Item 7</li></ul></html>'

navigation = Node('navigation.pt')
navigation.value = """<html><div class="batch" tal:define="batch news">
<a href="${batch.get_url(batch.prev)}" rel="prev">&laquo; Previous</a>

<a tal:condition="batch.num > batch.first+1"
   tal:content="batch.first" href="${batch.get_url(batch.first)}">1</a>
<a tal:condition="batch.num > batch.first+2">...</a>
<a tal:content="batch.prev" href="${batch.get_url(batch.prev)}"
   tal:condition="batch.num > batch.first" rel="prev">previousXX</a>
<a class="current" tal:content="batch.current">currentXX</a>
<a tal:content="batch.next" href="${batch.get_url(batch.next)}"
   tal:condition="batch.num < batch.last" rel="next">nextXX</a>
<a tal:condition="batch.num < batch.last-2">...</a>
<a tal:content="batch.last" href="${batch.get_url(batch.last)}"
   tal:condition="batch.num < batch.last-1">lastXX</a>

<a href="${batch.get_url(batch.next)}" rel="next">Next &raquo;</a>
</div></html>"""

navigation_1_render = """<html><div class="batch">
<a rel="prev">&laquo; Previous</a>




<a class="current">1</a>
<a href="item.2.html" rel="next">2</a>
<a>...</a>
<a href="item.4.html">4</a>

<a href="item.2.html" rel="next">Next &raquo;</a>\n</div></html>"""

navigation_2_render = """<html><div class="batch">
<a href="item.html" rel="prev">&laquo; Previous</a>



<a href="item.html" rel="prev">1</a>
<a class="current">2</a>
<a href="item.3.html" rel="next">3</a>

<a href="item.4.html">4</a>

<a href="item.3.html" rel="next">Next &raquo;</a>\n</div></html>"""

navigation_3_render = """<html><div class="batch">
<a href="item.2.html" rel="prev">&laquo; Previous</a>

<a href="item.html">1</a>

<a href="item.2.html" rel="prev">2</a>
<a class="current">3</a>
<a href="item.4.html" rel="next">4</a>



<a href="item.4.html" rel="next">Next &raquo;</a>\n</div></html>"""

navigation_4_render = """<html><div class="batch">
<a href="item.3.html" rel="prev">&laquo; Previous</a>

<a href="item.html">1</a>
<a>...</a>
<a href="item.3.html" rel="prev">3</a>
<a class="current">4</a>




<a rel="next">Next &raquo;</a>\n</div></html>"""


# data
data = {
    'news': [
        {'id': 'i1', 'name': 'Item 1'},
        {'id': 'i2', 'name': 'Item 2'},
        {'id': 'i3', 'name': 'Item 3'},
        {'id': 'i4', 'name': 'Item 4'},
        {'id': 'i5', 'name': 'Item 5'},
        {'id': 'i6', 'name': 'Item 6'},
        {'id': 'i7', 'name': 'Item 7'},
        ],
    }

view_str = f"""\
View('/templates')
├── ViewHtml('/templates/item.html', value='{template_1_render}')
├── ViewHtml('/templates/item.2.html', value='{template_2_render}')
├── ViewHtml('/templates/item.3.html', value='{template_3_render}')
└── ViewHtml('/templates/item.4.html', value='{template_4_render}')\
"""


def test_no_batch(tmp_path):
    template.name = 'template.pt'
    templates = Template.template_from_tree(root)
    view = templates.render(data)
    directory = tmp_path
    view.write(directory)

    assert tmp_path.exists()
    assert len(list(tmp_path.iterdir())) == 1
    template_html = tmp_path / 'template.html'
    assert template_html.exists()
    assert template_html.read_text() == template_render


def test_batch(tmp_path):
    template.name = 'item.tal_batch_news_2.pt'
    templates = Template.template_from_tree(root)
    view = templates.render(data)

    assert str(RenderTree(view)) == view_str.replace('</li>\n', '</li>\\n')

    directory = tmp_path
    view.write(directory)

    assert tmp_path.exists()
    assert len(list(tmp_path.iterdir())) == 4
    template_1_html = tmp_path / 'item.html'
    assert template_1_html.exists()
    assert template_1_html.read_text() == template_1_render
    template_2_html = tmp_path / 'item.2.html'
    assert template_2_html.exists()
    assert template_2_html.read_text() == template_2_render
    template_3_html = tmp_path / 'item.3.html'
    assert template_3_html.exists()
    assert template_3_html.read_text() == template_3_render
    template_4_html = tmp_path / 'item.4.html'
    assert template_4_html.exists()
    assert template_4_html.read_text() == template_4_render


def test_batch_navigation(tmp_path):
    root = Node('templates')
    navigation.parent = root
    navigation.name = 'item.tal_batch_news_2.pt'
    templates = Template.template_from_tree(root)
    view = templates.render(data)

    directory = tmp_path
    view.write(directory)

    assert tmp_path.exists()
    assert len(list(tmp_path.iterdir())) == 4
    template_1_html = tmp_path / 'item.html'
    assert template_1_html.exists()
    assert template_1_html.read_text() == navigation_1_render
    template_2_html = tmp_path / 'item.2.html'
    assert template_2_html.exists()
    assert template_2_html.read_text() == navigation_2_render
    template_3_html = tmp_path / 'item.3.html'
    assert template_3_html.exists()
    assert template_3_html.read_text() == navigation_3_render
    template_4_html = tmp_path / 'item.4.html'
    assert template_4_html.exists()
    assert template_4_html.read_text() == navigation_4_render


value1 = '<html><ul><li>1</li>\\n<li>2</li></ul></html>'
value2 = '<html><ul><li>3</li></ul></html>'
data_structure_render = f"""CollectionView('/item.tal_batch_a:b:c_2.pt')
├── ViewHtml('/item.tal_batch_a:b:c_2.pt/item.html', value='{value1}')
└── ViewHtml('/item.tal_batch_a:b:c_2.pt/item.2.html', value='{value2}')"""


def test_batch_data_structure():
    data = {'a': {'b': {'c': [1, 2, 3]}}}

    template = Node('item.tal_batch_a:b:c_2.pt')
    template.value = '<html><ul><li tal:repeat="i a.b.c">${i}</li></ul></html>'
    templates = Template.template_from_tree(template)
    view = templates.render(data)

    assert str(RenderTree(view)) == data_structure_render


def test_batch_data_json():
    data_json = Json(name='data.json', value=json.dumps(data))

    template.name = 'item.tal_batch_news_2.pt'
    templates = Template.template_from_tree(root)
    view = templates.render(data_json)

    assert str(RenderTree(view)) == view_str.replace('</li>\n', '</li>\\n')
