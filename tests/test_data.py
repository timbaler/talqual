from anytree import Node, RenderTree

from talqual.data import Data
from talqual.templates import HtmlPt, TalCommand


# data
root = Node('data')  # root folder
yaml = Node('data.yaml', parent=root)  # yaml file
yaml.value = 'variable1: Template rendered'
rst = Node('data2.rst', parent=root)  # rst file
rst.value = ':var1: testing'
json = Node('data3.json', parent=root)  # json file
json.value = '{"var1": "testing"}'
media = Node('media', parent=root)  # folder
image = Node('image.png', parent=media)  # image
image.value = b'\x89PNG'
pdf = Node('file.pdf', parent=media)  # pdf
pdf.value = 'pdf'

# templates
template = HtmlPt('template.pt')
template.value = '<html><p tal:content="data.variable1">Test</p></html>'
template_render = '<html><p>Template rendered</p></html>'
template_img = HtmlPt('template.pt')
template_img.value = '\
<html><p tal:content="media[\'file.pdf\'].value">Test</p></html>'
template_img_render = '<html><p>pdf</p></html>'
template_replace = TalCommand('img.tal_replace_media:image.png:.png', value='')


root_str = f"""\
Node('/data')
├── Node('/data/data.yaml', value='{yaml.value}')
├── Node('/data/data2.rst', value='{rst.value}')
├── Node('/data/data3.json', value='{json.value}')
└── Node('/data/media')
    ├── Node('/data/media/image.png', value=b'\\x89PNG')
    └── Node('/data/media/file.pdf', value='pdf')"""

data_str = f"""\
Data('/data')
├── Yaml('/data/data', value='{yaml.value}')
├── Rst('/data/data2', value='{rst.value}')
├── Json('/data/data3', value='{json.value}')
└── Data('/data/media')
    ├── Data('/data/media/image.png', value=b'\\x89PNG')
    └── Data('/data/media/file.pdf', value='pdf')"""


def test_data():
    assert str(RenderTree(root)) == root_str
    data = Data.data_from_tree(root)
    assert str(RenderTree(data)) == data_str

    # yaml
    assert data['data']['variable1'] == 'Template rendered'
    # rst
    assert data['data2']['var1'] == 'testing'
    # json
    assert data['data3']['var1'] == 'testing'

    # folder
    assert data['media']
    # image
    assert data['media']['image.png'].value == b'\x89PNG'
    # pdf
    assert data['media']['file.pdf'].value == 'pdf'

    # render template
    view = template.render(data)
    assert view.value == template_render

    # template with data file value
    view = template_img.render(data)
    assert view.value == template_img_render
    # template with data file for replacing
    view = template_replace.render(data)
    view_file = view.children[0]
    assert view_file.name == 'img.png'
    assert view_file.value == b'\x89PNG'


def test_root_data_only_one_file():
    data = Data.data_from_tree(yaml)
    assert data['variable1'] == 'Template rendered'
    # render template
    original_template = template.value
    template.value = template.value.replace('data.variable1', 'variable1')
    view = template.render(data)
    template.value = original_template
    assert view.value == template_render
