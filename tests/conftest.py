import sys

import pytest


driver = pytest.mark.skipif('--driver' not in sys.argv,
                            reason='Running without selenium')


@pytest.fixture
def firefox_options(firefox_options):
    firefox_options.add_argument('-headless')
    return firefox_options
