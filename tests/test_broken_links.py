import logging

from anytree import Node

from talqual.parsers.html import get_all_href
from talqual.templates import Template


# data
data = {}

# templates
root = Node('templates')
index = Node('index.html', parent=root)  # static html
index.value = '<html><body><a href="a.html"></a></body></html>'
index_value = index.value
a = Node('a.html', parent=root)  # static html
a.value = ('<html><body><a href="a.html"></a>'
           '<a href="folder/b.html"></a></body></html>')
a_value = a.value
folder = Node('folder', parent=root)
b = Node('b.html', parent=folder)  # static html
b.value = '<html><body><a href="../a.html"></a></body></html>'
b_value = b.value
c = Node('c.html', parent=folder)  # static html
c.value = '<html><body><a href="b.html"></a></body></html>'
c_value = c.value

# templates base href
index2_value = ('<html><base href="."><body>'
                '<a href="a.html"></a></body></html>')
a2_value = ('<html><base href="."><body><a href="a.html"></a>'
            '<a href="folder/b.html"></a></body></html>')
b2_value = '<html><base href="../"><body><a href="a.html"></a></body></html>'
c2_value = ('<html><base href="../"><body>'
            '<a href="folder/b.html"></a></body></html>')

# templates base href inverted
index3_value = ('<html><base href="folder/"><body>'
                '<a href="../a.html"></a></body></html>')
a3_value = ('<html><base href="folder/"><body><a href="../a.html"></a>'
            '<a href="b.html"></a></body></html>')
b3_value = '<html><base href="."><body><a href="../a.html"></a></body></html>'
c3_value = ('<html><base href="../folder/"><body>'
            '<a href="b.html"></a></body></html>')

# templates with broken links
index4_value = '<html><body><a href="b.html"></a></body></html>'
a4_value = ('<html><body><a href="a.html"></a>'
            '<a href="b.html"></a></body></html>')
b4_value = '<html><body><a href="../../a.html"></a></body></html>'
c4_value = '<html><body><a href="b.html#fail"></a></body></html>'

# links with query and fragments
d_root = Node('templates')
d_index = Node('index.html', parent=d_root)  # static html
d_index.value = ('<html><body><a id="test" href="index.html?k=v"></a>'
                 '<a href="index.html#test"></a>'
                 '<a href="index.html?k=v#test"></a>'
                 '<a href="video.mp4#t=10,20"></a></body></html>')
d_a = Node('a.html', parent=d_root)  # static html
d_a.value = ('<html><body><a href="index.html?k=v"></a>'
             '<a href="index.html#test"></a>'
             '<a href="index.html?k=v#test"></a>'
             '<a href="video.mp4#t=10,20"></a></body></html>')
d_video = Node('video.mp4', parent=d_root)  # video

# internal links variations
v_root = Node('templates')
v_index = Node('index.html', parent=v_root)  # static html
v_index.value = ('<html><body><a href="."></a><a href=".."></a>'
                 '<a href="/"></a><a href="#"></a></body></html>')

# external links
e_root = Node('templates')
e_index = Node('index.html', parent=e_root)  # static html
e_index.value = '<html><body><a href="//example.test"></a</body></html>'


def _check_links_common(root, caplog):
    templates = Template.template_from_tree(root)
    view = templates.render(data)
    with caplog.at_level(logging.DEBUG):
        errors = view.check_broken_links()

    assert 0 == errors
    assert 4 == caplog.text.count('\n')
    assert ('Checked links in index.html. Broken internal 0/1. '
            'External 0: all not checked.') in caplog.text
    assert ('Checked links in a.html. Broken internal 0/2. '
            'External 0: all not checked.') in caplog.text
    assert ('Checked links in folder/b.html. Broken internal 0/1. '
            'External 0: all not checked.') in caplog.text
    assert ('Checked links in folder/c.html. Broken internal 0/1. '
            'External 0: all not checked.') in caplog.text


def test_broken_links_ok(caplog):
    index.value = index_value
    a.value = a_value
    b.value = b_value
    c.value = c_value

    _check_links_common(root, caplog)


def test_broken_links_base_href(tmp_path, caplog):
    index.value = index2_value
    a.value = a2_value
    b.value = b2_value
    c.value = c2_value

    _check_links_common(root, caplog)


def test_broken_links_base_href_inverted(tmp_path, caplog):
    index.value = index3_value
    a.value = a3_value
    b.value = b3_value
    c.value = c3_value

    _check_links_common(root, caplog)


def test_broken_links_nok(caplog):
    index.value = index4_value
    a.value = a4_value
    b.value = b4_value
    c.value = c4_value

    templates = Template.template_from_tree(root)
    view = templates.render(data)
    with caplog.at_level(logging.DEBUG):
        errors = view.check_broken_links()

    assert 4 == errors
    assert 8 == caplog.text.count('\n')
    assert 'Broken internal link b.html in index.html' in caplog.text
    assert ('Checked links in index.html. Broken internal 1/1. '
            'External 0: all not checked.') in caplog.text
    assert 'Broken internal link b.html in a.html' in caplog.text
    assert ('Checked links in a.html. Broken internal 1/2. '
            'External 0: all not checked.') in caplog.text
    assert 'Broken internal link ../../a.html in folder/b.html' in caplog.text
    assert ('Checked links in folder/b.html. Broken internal 1/1. '
            'External 0: all not checked.') in caplog.text
    assert ('Broken internal link fragment b.html#fail '
            'in folder/c.html') in caplog.text
    assert ('Checked links in folder/c.html. Broken internal 1/1. '
            'External 0: all not checked.') in caplog.text


def test_broken_links_query_fragment(tmp_path, caplog):
    templates = Template.template_from_tree(d_root)
    view = templates.render(data)
    with caplog.at_level(logging.DEBUG):
        errors = view.check_broken_links()

    assert 0 == errors
    assert 4 == caplog.text.count('\n')
    assert ('NotImplemented to check fragment video.mp4#t=10,20 '
            'in index.html') in caplog.text
    assert ('Checked links in index.html. Broken internal 0/4. '
            'External 0: all not checked.') in caplog.text
    assert ('NotImplemented to check fragment video.mp4#t=10,20 '
            'in a.html') in caplog.text
    assert ('Checked links in a.html. Broken internal 0/4. '
            'External 0: all not checked.') in caplog.text


def test_broken_links_variations(caplog):
    templates = Template.template_from_tree(v_root)
    view = templates.render(data)

    errors = view.check_broken_links()
    assert 0 == errors


def test_broken_links_external(caplog):
    templates = Template.template_from_tree(e_root)
    view = templates.render(data)
    with caplog.at_level(logging.DEBUG):
        errors = view.check_broken_links()

    assert 0 == errors
    assert 1 == caplog.text.count('\n')
    assert ('Checked links in index.html. Broken internal 0/0. '
            'External 1: all not checked.') in caplog.text


def test_broken_links_other():
    hrefs = list(get_all_href('<a href="javascript:void(0)"></a>'))
    assert len(list(hrefs)) == 1
    assert ('other', 'javascript:void(0)', None) in hrefs


def test_base_href():
    # internal links variations
    hrefs = list(get_all_href(v_index.value))
    assert len(hrefs) == 4
    assert ('internal', '.', '') in hrefs
    assert ('internal', '..', '') in hrefs
    assert ('internal', '/', '') in hrefs
    assert ('internal', '', '') in hrefs

    # internal link with internal base href
    v = '<html><base href="../"><body><a href="a.html"></a></body></html>'
    hrefs = list(get_all_href(v))
    assert len(hrefs) == 1
    assert ('internal', '../a.html', '') in hrefs

    # internal link with internal base href, without trailing /
    v = '<html><base href=".."><body><a href="a.html"></a></body></html>'
    hrefs = list(get_all_href(v))
    assert len(hrefs) == 1
    assert ('internal', '../a.html', '') in hrefs

    # internal link converts to external by base href
    v = '<html><base href="//x.test"><body><a href="a.html"></a></body></html>'
    hrefs = list(get_all_href(v))
    assert len(hrefs) == 1
    assert ('external', '//x.test/a.html', None) in hrefs
