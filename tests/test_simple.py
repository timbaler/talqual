from anytree import Node, RenderTree

from talqual.importer import FileSystemImporter
from talqual.templates import Folder, Template


# templates
root = Node('templates')
index = Node('index.html', parent=root)  # static html
index.value = '<html><title>Testing</title><body><p>Test</p></body></html>'
hidden = Node('_hidden.txt', parent=root)  # hidden file
template = Node('template.pt', parent=root)  # simple template
template.value = '<html><p tal:content="variable1">Test</p></html>'
template_render = '<html><p>Template rendered</p></html>'
macros = Node('macros', parent=root)  # templates with macros
macro_base = Node('_base.pt', parent=macros)
macro_base.value = (
    '<html metal:define-macro="master"><title>${variable1}</title>'
    '<body><div metal:define-slot="content"></div></body></html>')
macro_index = Node('index.pt', parent=macros)
macro_index.value = (
    '<html metal:use-macro="load: macros/_base.pt"><body>'
    '<div metal:fill-slot="content"><p>${variable1}</p></div></body></html>')
macro_index_render = '<html><title>Template rendered</tit\
le><body><div><p>Template rendered</p></div></body></html>'
news = Node('news', parent=root)  # folder with repeated elements
n = Node('new.tal_repeat_news.pt', parent=news)
n.value = '<html><p tal:content="tal_repeat_news.item.content"></p></html>'
n1_render = '<html><p>First news</p></html>'
n2_render = '<html><p>Second news</p></html>'
media = Node('media', parent=root)  # folder
image = Node('image.png', parent=media)  # image
image.value = b'\x89PNG'
pdf = Node('file.pdf', parent=media)  # pdf
pdf.value = 'pdf'

# data
data = {
    'variable1': 'Template rendered',
    'news': [
        {'name': 'n1', 'content': 'First news'},
        {'name': 'n2', 'content': 'Second news'},
        ],
    }

# tree view
root_str = f"""\
Node('/templates')
├── Node('/templates/index.html', value='{index.value}')
├── Node('/templates/_hidden.txt')
├── Node('/templates/template.pt', value='{template.value}')
├── Node('/templates/macros')
│   ├── Node('/templates/macros/_base.pt', value='{macro_base.value}')
│   └── Node('/templates/macros/index.pt', value='{macro_index.value}')
├── Node('/templates/news')
│   └── Node('/templates/news/new.tal_repeat_news.pt', value='{n.value}')
└── Node('/templates/media')
    ├── Node('/templates/media/image.png', value=b'\\x89PNG')
    └── Node('/templates/media/file.pdf', value='pdf')\
"""

root2_str = f"""\
Node('/templates')
├── Node('/templates/image.png', value=b'\\x89PNG')
└── Node('/templates/index.html', value=b'{template.value}')"""

template_str = f"""\
Folder('/templates')
├── HtmlPt('/templates/index.html', value='{index.value}')
├── NoView('/templates/_hidden.txt')
├── HtmlPt('/templates/template.pt', value='{template.value}')
├── Folder('/templates/macros')
│   ├── NoView('/templates/macros/_base.pt', value='{macro_base.value}')
│   └── HtmlPt('/templates/macros/index.pt', value='{macro_index.value}')
├── Folder('/templates/news')
│   └── TalCommand('/templates/news/new.tal_repeat_news.pt', value='{n.value}')
└── Folder('/templates/media')
    ├── Template('/templates/media/image.png', value=b'\\x89PNG')
    └── Template('/templates/media/file.pdf', value='pdf')\
"""

view_str = f"""\
View('/templates')
├── ViewHtml('/templates/index.html', value='{index.value}')
├── ViewHtml('/templates/template.html', value='{template_render}')
├── View('/templates/macros')
│   └── ViewHtml('/templates/macros/index.html', value='{macro_index_render}')
├── View('/templates/news')
│   ├── ViewHtml('/templates/news/new.0.html', value='{n1_render}')
│   └── ViewHtml('/templates/news/new.1.html', value='{n2_render}')
└── View('/templates/media')
    ├── View('/templates/media/image.png', value=b'\\x89PNG')
    └── View('/templates/media/file.pdf', value='pdf')\
"""


def test_simple(tmp_path):
    # print('Template directory')
    # print('------------------')
    # print(RenderTree(root))

    assert str(RenderTree(root)) == root_str

    # print('Template Tree')
    # print('-------------')
    templates = Template.template_from_tree(root)
    # print(RenderTree(templates))

    assert str(RenderTree(templates)) == template_str

    # print('View Tree')
    # print('-------------')
    view = templates.render(data)
    # print(RenderTree(view))

    assert str(RenderTree(view)) == view_str

    directory = tmp_path
    view.write(directory)

    assert tmp_path.exists()
    index_html = tmp_path / 'index.html'
    assert index_html.exists()
    assert index_html.read_text() == index.value
    assert not (tmp_path / '_hidden.txt').exists()
    template = tmp_path / 'template.html'
    assert template.exists()
    assert template.read_text() == template_render
    macros = tmp_path / 'macros'
    assert macros.exists()
    assert not (macros / '_base.html').exists()
    assert not (macros / '_base.pt').exists()
    template = macros / 'index.html'
    assert template.exists()
    assert template.read_text() == macro_index_render
    news = tmp_path / 'news'
    assert news.exists()
    assert len(list(news.iterdir())) == 2
    assert not (news / 'news.tal_repeat_news.html').exists()
    n1 = news / 'new.0.html'
    n2 = news / 'new.1.html'
    assert n1.exists()
    assert n2.exists()
    assert n1.read_text() == n1_render
    assert n2.read_text() == n2_render
    media = tmp_path / 'media'
    assert media.exists()
    assert (media / 'image.png').exists()
    pdf = media / 'file.pdf'
    assert pdf.exists()
    assert pdf.read_text() == 'pdf'


def test_simple_from_filesystem(tmp_path):
    templates = tmp_path / 'templates'
    templates.mkdir()
    index = templates / 'index.html'
    index.write_text(template.value)
    img = templates / 'image.png'
    img.write_bytes(b'\x89PNG')

    importer = FileSystemImporter()
    root = importer.import_(templates)

    def order(items):
        return sorted(items, key=lambda item: item.name)

    assert str(RenderTree(root, childiter=order)) == root2_str

    templates = Template.template_from_tree(root)
    view = templates.render(data)
    output = tmp_path / 'output'
    output.mkdir()
    view.write(output)

    assert output.exists()
    index_html = output / 'index.html'
    assert index_html.exists()
    assert index_html.read_text() == template_render
    img_html = output / 'image.png'
    assert img_html.exists()
    assert img_html.read_bytes() == b'\x89PNG'
    assert img_html.stat().st_nlink == 2  # is a hardlink
    assert img.stat().st_ino == img_html.stat().st_ino  # hardlinks

    # render another time with same contents
    view.write(output)
    assert img_html.read_bytes() == b'\x89PNG'
    assert img_html.stat().st_nlink == 2  # is a hardlink
    assert img.stat().st_ino == img_html.stat().st_ino  # hardlinks

    # change file contents
    img.unlink()
    img.write_bytes(b'\x89PNG2')
    view.write(output)
    assert img_html.read_bytes() == b'\x89PNG2'
    assert img_html.stat().st_nlink == 2  # is a hardlink
    assert img.stat().st_ino == img_html.stat().st_ino  # hardlinks


def test_empty_folder(tmp_path):
    templates = Folder('folder')
    view = templates.render(data)

    output = tmp_path / 'output'
    output.mkdir()
    view.write(output)

    assert output.exists()
    folder_output = output / 'folder'
    assert not folder_output.exists()
