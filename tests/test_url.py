import logging

import pytest
from anytree import Node, search

from talqual.templates import Folder, HtmlPt, Template
from talqual.zpt import BaseTemplate


# data
data = {}

# templates
root = Node('templates')
macro = Node('_master.pt', parent=root)  # macro
macro.value = ('<html metal:define-macro="master"><body>'
               '<a href="${url: index.html}"></a>'
               '<a href="${url: folder/a.html}"></a>'
               '<main metal:define-slot="main"></main></body></html>')
url_macro_value = macro.value
error_macro_value = macro.value.replace('${url: ', '').replace('}', '')
index = Node('index.html', parent=root)  # html template
index.value = ('<html metal:use-macro="load: _master.pt">'
               '<main metal:fill-slot="main"><a href="folder/a.html"></a>'
               '</main></html>')
index_render = ('<html><body><a href="index.html"></a><a href="folder/a.html">'
                '</a><main><a href="folder/a.html"></a></main></body></html>')
folder = Node('folder', parent=root)
a = Node('a.html', parent=folder)  # html template
a.value = ('<html metal:use-macro="load: _master.pt">'
           '<main metal:fill-slot="main"><a href="../index.html"></a>'
           '</main></html>')
a_render = ('<html><body><a href="../index.html"></a>'
            '<a href="../folder/a.html"></a><main>'
            '<a href="../index.html"></a></main></body></html>')

# templates inverted
root2 = Node('templates')
index2 = Node('index.html', parent=root2)  # html template
index2.value = ('<html metal:use-macro="load: folder/_master.pt">'
                '<main metal:fill-slot="main"><a href="folder/a.html"></a>'
                '</main></html>')
folder2 = Node('folder', parent=root2)
a2 = Node('a.html', parent=folder2)  # html template
a2.value = ('<html metal:use-macro="load: folder/_master.pt">'
            '<main metal:fill-slot="main"><a href="../index.html"></a>'
            '</main></html>')
macro2 = Node('_master.pt', parent=folder2)  # macro
macro2.value = ('<html metal:define-macro="master"><body>'
                '<a href="${url: index.html}"></a>'
                '<a href="${url: folder/a.html}"></a>'
                '<main metal:define-slot="main"></main></body></html>')


def test_page_template():
    p = BaseTemplate('')

    assert p.expression_types['load']
    assert p.expression_types['url']
    with pytest.raises(NotImplementedError):
        p.loader_search('a.html')


def test_url_relative(caplog):
    macro.value = error_macro_value
    templates = Template.template_from_tree(root)
    view = templates.render(data)

    with caplog.at_level(logging.WARNING):
        errors = view.check_broken_links()

    assert errors == 2
    assert 'Broken internal link index.html in folder/a.html' in caplog.text
    assert 'Broken internal link folder/a.html in folder/a.html' in caplog.text

    macro.value = url_macro_value
    templates = Template.template_from_tree(root)
    view = templates.render(data)
    assert 0 == view.check_broken_links()

    view_index = search.find_by_attr(view, 'index.html', 'name')
    view_a = search.find_by_attr(view, 'a.html', 'name')
    assert view_index.value == index_render
    assert view_a.value == a_render


def test_url_relative_inverted():
    templates = Template.template_from_tree(root2)
    view = templates.render(data)
    assert 0 == view.check_broken_links()

    view_index = search.find_by_attr(view, 'index.html', 'name')
    view_a = search.find_by_attr(view, 'a.html', 'name')
    assert view_index.value == index_render
    assert view_a.value == a_render


def test_url_relative_shared_paths():
    t = Folder('')
    i = HtmlPt('i.html', parent=t)
    i.value = ''

    f0 = Folder('f0', parent=t)
    i1 = HtmlPt('i1.html', parent=f0)
    i1.value = ''

    f1 = Folder('f1', parent=t)
    f2 = Folder('f2', parent=f1)

    f3 = Folder('f3', parent=f2)
    a = HtmlPt('a.html', parent=f3)
    a.value = '<html><a href="${url: f1/f2/f4/b.html}"></a></html>'
    a_render = '<html><a href="../f4/b.html"></a></html>'

    f4 = Folder('f4', parent=f2)
    b = HtmlPt('b.html', parent=f4)
    b.value = '<html><a href="${url: f1/f2/f4/b.html}"></a></html>'
    b_render = '<html><a href=""></a></html>'

    assert a.relative_url('f1/f2/f3/a.html') == ''
    assert a.relative_url('f1/f2/f3') == './'
    assert a.relative_url('f1/f2/f3/') == './'

    assert a.relative_url('f1/f2') == '../'
    assert a.relative_url('f1/f2/') == '../'

    assert a.relative_url('f1') == '../../'
    assert a.relative_url('f1/') == '../../'

    assert a.relative_url('f1/f2/f4/b.html') == '../f4/b.html'
    assert a.relative_url('f1/f2/f4') == '../f4'
    assert a.relative_url('f1/f2/f4/') == '../f4/'

    assert a.relative_url('f0') == '../../../f0'
    assert a.relative_url('f0/i1.html') == '../../../f0/i1.html'
    assert a.relative_url('i.html') == '../../../i.html'

    assert i.relative_url('f1/f2/f3/a.html') == 'f1/f2/f3/a.html'
    assert i1.relative_url('f1/f2/f3/a.html') == '../f1/f2/f3/a.html'

    view = t.render(data)
    assert 0 == view.check_broken_links()
    view_a = search.find_by_attr(view, 'a.html', 'name')
    assert view_a.value == a_render
    view_b = search.find_by_attr(view, 'b.html', 'name')
    assert view_b.value == b_render
