
from org.transcrypt.stubs.browser import __pragma__


__pragma__('skip')
document = window = 0  # Prevent complaints by optional static checker
__new__ = XMLHttpRequest = Date = 0
__pragma__('noskip')


def is_checked(element, filter_class):
    return (' '+element.className+' ').indexOf(' '+filter_class+' ') > -1


def style_hide(element):
    if element.style:
        element.style.display = 'none'
    else:
        element.style = {'display': 'none'}


def style_display(element):
    if element.style:
        element.style.display = ''


class Calendar:

    def __init__(self, data_class):
        """
        :param data_class: The data HTML element CLASS
        """
        self.data_class = data_class
        self.run()

    @property
    def data(self):
        return document.getElementsByClassName(self.data_class)

    def run(self):
        now = Date.now()
        for calendar in self.data:
            for entry in calendar.childNodes:
                if 'date' in entry.dataset:
                    if now < Date.parse(entry.dataset.date):
                        style_display(entry)
                    else:
                        style_hide(entry)


class Faceted:

    def __init__(self, data_class, batch_size=None, pagination_class=None):
        """
        :param data_class: The data HTML element CLASS
        :param int batch_size: The pagination (when faceted is batch) size
        :param pagination_class: The pagination (when batch) element CLASS
        """
        self.data_class = data_class
        self.batch_size = batch_size
        self.size = 0
        self.pagination_class = pagination_class
        self.batch_faceted = None
        self.batch_pagination = []
        self.values = None
        self.field_name = None
        self.next_url = None

    @property
    def data(self):
        return document.getElementsByClassName(self.data_class)

    def next_callback(self):
        if self.next_url is not None:
            self.batch_faceted.innerHTML = ''  # reset results
            return self._next_faceted(self.next_url)

    def _next_url(self, html):
        a_next = html.querySelector(
            '.' + self.pagination_class + ' [rel="next"]')
        if bool(a_next):
            return a_next.getAttribute('href')

    def _next_faceted(self, next_url):
        if next_url is None:
            return
        if self.size >= self.batch_size:
            # prepare next page
            self.next_url = next_url
            self.size = 0
            return

        self.next_url = None
        xhr = __new__(XMLHttpRequest())

        def parse_faceted():
            if xhr.readyState == 4 and xhr.status == 200:
                html = document.createElement('html')
                html.innerHTML = xhr.responseText
                faceted_html = html.getElementsByClassName(self.data_class)
                self.filter_field(faceted_html)
                self._next_faceted(self._next_url(html))

        xhr.onreadystatechange = parse_faceted
        xhr.open('GET', next_url)
        xhr.send()

    def init_filtering(self):
        if self.batch_faceted is not None:
            self.batch_faceted.innerHTML = ''
            return
        # hide originals
        for faceted in self.data:
            style_hide(faceted)
        paginations = document.querySelectorAll('.'+self.pagination_class)
        for pagination in paginations:
            style_hide(pagination)
        # copy empty for results
        original = self.data.item(0)
        faceted = original.cloneNode(False)
        style_display(faceted)
        original.insertAdjacentElement('afterend', faceted)
        self.batch_faceted = faceted
        # copy pagination next
        for pagination in paginations:
            pagination_clone = pagination.cloneNode(False)

            style_display(pagination_clone)
            pagination.insertAdjacentElement('afterend', pagination_clone)
            for link_next in pagination.querySelectorAll('[rel="next"]'):
                next_clone = link_next.cloneNode(True)
                next_clone.removeAttribute('href')
                next_clone.addEventListener('click', self.next_callback)
                pagination_clone.insertAdjacentElement('beforeend', next_clone)
            self.batch_pagination.append(pagination_clone)

    def end_filtering(self):
        # remove results
        self.batch_faceted.remove()
        self.batch_faceted = None
        # remove paginations
        for pagination in self.batch_pagination:
            pagination.remove()
        self.batch_pagination = []
        # display originals
        for faceted in self.data:
            style_display(faceted)
        paginations = document.querySelectorAll('.'+self.pagination_class)
        for pagination in paginations:
            style_display(pagination)

    def _select_element(self, element, select=True):
        if self.batch_faceted is not None:
            if select:
                # copy
                clone = element.cloneNode(True)
                self.batch_faceted.insertAdjacentElement('beforeend', clone)

        else:
            if select:
                style_display(element)
            else:
                style_hide(element)

    def _filter_one(self, faceted):
        for element in faceted.childNodes:
            if element.nodeType != 1:
                continue
            hide = bool(self.values)
            for value in self.values:
                filter_class = self.field_name+'-'+value
                if is_checked(element, filter_class):
                    hide = False
                    self.size += 1
                    break

            self._select_element(element, not hide)

            if self.batch_size and self.size >= self.batch_size:
                break

    def filter(self, field):
        """
        :param field: The form element requesting for filter

        """
        if len(field.selectedOptions) > 1:
            # multiselect
            self.values = [option.value for option in field.selectedOptions]
        elif field.value:
            self.values = [field.value]
        elif len(field.selected) > 0:
            self.values = field.selected
        else:
            self.values = []

        if self.batch_faceted is not None and not bool(self.values):
            self.end_filtering()
            return

        if self.batch_size:
            self.init_filtering()

        self.field_name = field.getAttribute('name')
        self.filter_field(self.data)

        if self.batch_faceted is not None:
            self._next_faceted(self._next_url(document))

    def filter_field(self, data):
        for faceted in data:
            if not faceted.isEqualNode(self.batch_faceted):
                self._filter_one(faceted)


class VirtualSelect():

    def __init__(self, name, faceted=None):
        self.name = name
        self.faceted = faceted
        self.selected = []

    def _call_onchange(self):
        if self.faceted:
            self.faceted.filter(self)

    @property
    def value(self):
        if self.selected:
            return self.selected[0]

    @property
    def selectedOptions(self):
        return [{'value': value} for value in self.selected]

    def getAttribute(self, attr):
        if attr == 'name':
            return self.name

    def reset(self):
        self.selected = []
        self._call_onchange()

    def toggle(self, value):
        if value in self.selected:
            self.selected.remove(value)
        else:
            self.selected.append(value)

        self._call_onchange()
