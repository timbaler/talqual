

class ExtraData():
    """
    :ivar _data: remember data rendered
    :ivar _extra_data: TAL data that is required when rendering `:class:HtmlPt`
    """

    def set_data(self, data):
        self._data = data

    def get_data(self):
        try:
            return self._data
        except AttributeError:
            return {}

    def set_extra_data(self, key, value):
        self._extra_data = {key: value}

    def get_extra_data(self):
        """Return extra_data from self or from parent"""
        try:
            return self._extra_data
        except AttributeError:
            if self.parent is not None:
                data_parent = self.parent.get_extra_data()
                if data_parent:
                    return data_parent
            return {}

    def full_path(self, include_root=True):
        """Return the full path from root to this node

        :rtype: str
        """
        if include_root:
            start = 0
        else:
            start = 1

        sep = self.separator
        return sep.join([""] + [str(node.name) for node in self.path[start:]])

    def relative_path(self, include_root=True):
        """Return the relative path from root to this node

        :rtype: str
        """
        path = self.full_path(include_root)
        while path.startswith('/'):
            path = path[1:]
        return path
