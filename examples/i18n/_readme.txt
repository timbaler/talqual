
First time:

pybabel extract -F _babel.cfg -o _locales/talqual.example.pot .
pybabel init -D talqual.example -i _locales/talqual.example.pot -d _locales -l ca
pybabel init -D talqual.example -i _locales/talqual.example.pot -d _locales -l en
pybabel init -D talqual.example -i _locales/talqual.example.pot -d _locales -l oc
pybabel compile -D talqual.example -d _locales

Updates:

pybabel extract -F _babel.cfg -o _locales/talqual.example.pot .
pybabel  update -D talqual.example -i _locales/talqual.example.pot -d _locales
pybabel compile -D talqual.example -d _locales
